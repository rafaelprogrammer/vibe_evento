import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

import { ParticipantsInterface } from '../../pages/participants/participants-interface';

@Injectable()
export class ParticipantsProvider {

  private api = 'http://receptivawebapi.azurewebsites.net/api/';

  constructor(public http: HttpClient) {
  }

  getParticipants(event_id: number) {
    return new Promise((resolve, reject) => {
      let url = this.api + `Evento/ParticipantesDoEvento?idEvento=${event_id}`;
      return this.http.get<ParticipantsInterface>(url)
          .subscribe((result: any) => {
            resolve(result);
          }, (error) => {
            reject(error)
          })
    })
  }


}
