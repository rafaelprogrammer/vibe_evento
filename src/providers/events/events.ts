import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import 'rxjs/add/operator/map';

@Injectable()
export class EventsProvider {

  private api = 'http://receptivawebapi.azurewebsites.net/api/';

  constructor(public http: HttpClient) {
  }

  getEvents() {
    return new Promise((resolve, reject) => {
      let url = this.api + 'Evento/EventosAtivosDoCliente?idCliente=1';
      this.http.get(url)
        .subscribe((result: any) => {
          resolve(result);
      },
        (error) => {
          reject(error);
      });

    });
  }


  getEvent(event_id: number) {
    return new Promise((resolve, reject) => {
      let url = this.api + `Evento/EventosAtivosDoCliente?idCliente=${event_id}`;
      this.http.get(url)
        .subscribe((result: any) => {
          resolve(result);
      },
        (error) => {
          reject(error);
      });

    });
  }

}
