import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
// import { filter } from 'rxjs/operators';

import { EventsProvider } from '../../../providers/events/events';
// import { ParticipantsProvider } from '../../../providers/participants/participants';
// import { ParticipantsInterface } from '../participants-interface';

@IonicPage()
@Component({
  selector: 'page-list-participants',
  templateUrl: 'list-participants.html',
})
export class ListParticipantsPage implements OnInit {

  event: any = {};
  event_id;
  // participants: ParticipantsInterface;

  constructor(public navCtrl: NavController, public navParams: NavParams, private eventService: EventsProvider) {
    this.event_id = this.navParams.get('event_id');
    // this.participants = [];
  }

  ngOnInit() {
    // this.participantsService.getParticipants(this.event_id)
    //       .then((result) => {
    //         console.log(result)
    //         // for(let i = 0; i < result.lista.length; i++) {
    //         //   let participant = result[i];
    //         //   console.log(participant);
    //         // }
    //       })
    this.getEvent();
  }

  getEvent() {
    this.eventService.getEvent(this.event_id)
          .then((result: any) => {
            return result.filter((item) => {
              this.event = item;
              console.log(item)
            })
          })
  }

}
