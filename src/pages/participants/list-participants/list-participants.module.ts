import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListParticipantsPage } from './list-participants';

@NgModule({
  declarations: [
    ListParticipantsPage,
  ],
  imports: [
    IonicPageModule.forChild(ListParticipantsPage),
  ],
})
export class ListParticipantsPageModule {}
