import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


import { EventsProvider } from '../../../providers/events/events';
import { EventsInterface } from '../events';

@IonicPage()
@Component({
  selector: 'page-list-events',
  templateUrl: 'list-events.html',
})
export class ListEventsPage implements OnInit {

  // events: Array<EventsInterface[]>;
  events: EventsInterface[];

  constructor(public navCtrl: NavController, public navParams: NavParams, private eventService: EventsProvider) {
    this.events = [];
  }

  ngOnInit() {
      this.getEvents();
  }

  getEvents() {
    this.eventService.getEvents()
          .then((result: any[]) => {
            for(var i = 0; i < result.length; i++) {
              let event = result[i]
              this.events.push(event) 
            }
          })
          .catch((error: any) => {
            console.log(error)
          })
  }


  itemTapped(event, item) {
    // That's right, we're pushing to ourselves!
    this.navCtrl.push('DetailsEventPage', {
      item: item
    });
  }


}
