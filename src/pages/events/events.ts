export interface EventsInterface {

  Id?: number;
  Nome: string;
  Quando: string;
  Inicio: string;
  Local: string;
  ClienteImagem: string;
  CapturarAssinatura: string;
  Imagem: string;
  UsarImpressora: string;

}
