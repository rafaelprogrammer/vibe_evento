import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { EventsInterface } from '../events';

@IonicPage()
@Component({
  selector: 'page-details-event',
  templateUrl: 'details-event.html',
})
export class DetailsEventPage {

  event: Array<EventsInterface[]>;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.event = this.navParams.get('item'); 
  }

  listParticipants(event_id) {
    this.navCtrl.setRoot('ListParticipantsPage', {
      event_id: event_id
    })
  }

}
